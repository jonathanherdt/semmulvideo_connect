'use strict';

var marmotta = MarmottaClient('http://37.120.177.56:8080/marmotta/');

/**
 * Video annotation according to the Open Annotation standard.
 * For more information, see http://www.openannotation.org and https://www.w3.org/community/openannotation/
 * Also uses media fragments to identify time and location of the annotation
 * For more information, see http://www.w3.org/TR/media-frags/ and http://www.openannotation.org/spec/core/core.html#FragmentURIs
 */
function AnnotationClient(){
  var sm = 'http://37.120.177.56/semanticmultimedia/';
  var nifCore = 'http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#';
  var foaf = 'http://xmlns.com/foaf/0.1/';
  var prefixes = 'PREFIX sm: <' + sm + '> \n\
    PREFIX oa: <http://www.openannotation.org/ns/> \n\
    PREFIX dctypes: <http://purl.org/dc/dcmitype/> \n\
    PREFIX dc: <http://purl.org/dc/terms/> \n\
    PREFIX cnt: <http://www.w3.org/2011/content#> \n\
    PREFIX foaf: <' + foaf + '> \n\
    PREFIX nif: <' + nifCore + '> \n';

  var sendSparqlSelectCommand = function(command, callbackMethod){
    console.log(command);
    // Replace all tabs, spaces, and newline characters
    command = command.replace(/[\s]+/g, ' ');
    marmotta.sparqlClient.select( command, callbackMethod);
  };

  var sendSparqlUpdateCommand = function(command, callbackMethod){
    console.log(command);
    // Replace all tabs, spaces, and newline characters
    command = command.replace(/[\s]+/g, ' ');
    marmotta.sparqlClient.update( command, callbackMethod);
  };
  /**
   * @param videoURL    The video to be annotated
   * @param begintime   OPTIONAL: The time in seconds at which the annotation should begin appearing
   *                    If begintime is unspecified, the tag will be applied without a timestamp.
   *                    This could be interpreted as a tag for the whole video.
   * @param endtime     The time in seconds at which the annotation should end appearing
   * @param trackNumber The identifier of the annotation track that is used to display the annotation
                        (Similar, but not to be confused with audio track)
   * @param content     The content of the annotation
   * @param tag         Boolean value that shows whether the content of the annotation is a tag.
   *                    In this case, content should be made up of a URI to a semantic web entity.
   * @param author      The author of the annotation
   * @param location    OPTIONAL: a location object with top and left, as well as
   *                    width and height values, all in the form of percentage values.
   *                    Please pass in as undefined when not used.
   * @param selectors   OPTIONAL: Use when string contains substrings that separately refer to
   *                    resources. Expects an array with objects that should be filled with the
   *                    following parameters:
   *                    @param beginIndex   Begin index of the substring in the original string
   *                    @param endIndex     End index of the substring in the original string
   *                    @param content      The substring itself
   *                    @param reference    The reference this substring refers to in form of a URI
   */
  this.addAnnotation = function(videoURL, begintime, endtime, trackNumber, content, tag, author, location, selectors){
    // Find the highest annotation ID. This ID + 1 will be the ID of the new annotation.
    var getHighestAnnotationID = prefixes + 'SELECT (MAX(?annotationID) as ?highestAnnotationID) WHERE\n\
      { ?annotation rdf:type oa:Annotation.\n ' +
      // Strip the beginning part of the annotation and convert the rest to an integer
'       BIND(xsd:integer(REPLACE(?annotation, "' + sm + 'Annotation", "", "i")) AS ?annotationID).  }';
    sendSparqlSelectCommand(getHighestAnnotationID, function(result){
      var newAnnotationID;
      // Take care of the fact that there might be no annotations so far.
      if(result.length > 0){
        newAnnotationID = Number(result[0].highestAnnotationID.value) + 1;
      } else {
        newAnnotationID = 0;
      }
      // From here on out, turtle syntax is used.
      // Please see documentation for how an annotation is built.
      var updateCommand = prefixes + 'INSERT DATA{ \n\
        sm:Annotation' + newAnnotationID + ' rdf:type oa:Annotation; \n\
          oa:annotatedBy sm:author' + author + ' ; \n\
          oa:annotatedAt "' + (new Date).toISOString() + '"; \n\
          oa:serializedAt "' + (new Date).toISOString() + '"; \n\
          oa:hasTarget <' + videoURL;
          if(begintime !== undefined){
            updateCommand += '#t=' + begintime + ',' + endtime
            + (location !== undefined? ',%x' + location.top     + '.' + location.left + '.'
            + location.width + '.' + location.height : '');
          }
          updateCommand += '>; \n\
          oa:hasBody sm:Body' + newAnnotationID + ' . \n\
        <' + videoURL + '> rdf:type dctypes:MovingImage . \n\
        sm:Annotation' + newAnnotationID + ' sm:track ' + trackNumber + ' . \n';
      if (tag) {
        updateCommand += 'sm:Annotation' + newAnnotationID + ' oa:motivatedBy oa:tagging . \n\
          sm:Body' + newAnnotationID + ' rdf:type oa:SemanticTag ; \n\
          foaf:page <' + content + '> . \n';
      } else {
        updateCommand += 'sm:Body' + newAnnotationID + ' \
              rdf:type cnt:ContentAsText, dctypes:Text; \n\
              dc:format "text/plain" ; \n';
        updateCommand += 'cnt:chars "' + content + '" . \n';
        if(selectors !== undefined){
          updateCommand += 'sm:Body' + newAnnotationID + ' oa:hasSource <' + sm + 'Source' + newAnnotationID + '#char=0> . \n\
            <' + sm + 'Source' + newAnnotationID + '#char=0> nif:isString "' + content + '" ; \n\
                                                    nif:beginIndex 0 ; \n\
                                                    nif:endIndex ' + content.length + ' ; \n\
                                                    rdf:type nif:Context, nif:RFC5147String . \n\
          ';
          for(var i = 0; i < selectors.length; i++){
            // Insert every selector separately. The URI of the new selector is made up of
            // the annotation ID, as well as the index of the item in the selectors array.
            updateCommand += 'sm:Body' + newAnnotationID + ' oa:hasSelector <' + sm + 'Selector' + newAnnotationID + '_' + i + '#char=' + selectors[i].beginIndex + ',' + selectors[i].endIndex + '> . \n\
              <' + sm + 'Selector' + newAnnotationID + '_' + i + '#char=' + selectors[i].beginIndex + ',' + selectors[i].endIndex + '> rdf:type oa:FragmentSelector, nif:Phrase, nif:RFC5147String ; \n\
                nif:beginIndex ' + selectors[i].beginIndex + ' ; \n\
                nif:endIndex ' + selectors[i].endIndex + ' ; \n\
                nif:anchorOf "' + content.substring(selectors[i].beginIndex, selectors[i].endIndex) + '" ; \n\
                foaf:page <' +selectors[i].reference + '> . \n\
            ';
          }
        }
      }
      updateCommand += '}';

      sendSparqlUpdateCommand (updateCommand, function(result){});
    });
  };

  this.deleteAnnotation = function(annotationString) {
    // Deletion has to be requested twice: The first DELETE is for NIF enhanced content,
    // the second for regular content.
    var deletionCommand = prefixes +
      'DELETE { \n\
         ' + annotationString + ' ?annotationProperty ?annotationObject . \n\
         ?body ?bodyProperty ?bodyObject . \n\
         ?source ?sourceProperty ?sourceObject . \n\
         ?selector ?selectorProperty ?selectorObject . \n\
       } WHERE { \n\
         ' + annotationString + ' ?annotationProperty ?annotationObject ; \n\
                                  oa:hasBody ?body . \n\
         ?body ?bodyProperty ?bodyObject ; \n\
               oa:hasSource ?source ; \n\
               oa:hasSelector ?selector . \n\
         ?source ?sourceProperty ?sourceObject . \n\
         ?selector ?selectorProperty ?selectorObject . \n\
       } ; \n\
       DELETE { \n\
         ' + annotationString + ' ?annotationProperty ?annotationObject . \n\
         ?body ?bodyProperty ?bodyObject . \n\
       } WHERE { \n\
         ' + annotationString + ' ?annotationProperty ?annotationObject ; \n\
                                  oa:hasBody ?body . \n\
         ?body ?bodyProperty ?bodyObject . \n\
       } ';
    sendSparqlUpdateCommand(deletionCommand , function(result){});
  };

  /**
   * Delete the annotation with the given id.
   * @param annotationID  The id of the annotation to be deleted.
   */
  this.deleteAnnotationWithId = function(annotationID){
    this.deleteAnnotation('sm:Annotation' + annotationID);
  };

  this.deleteAllAnnotations = function(){
    this.deleteAnnotation('?anno');
  };

  /**
   * Load all annotations for the specified video.
   * Note: If no videoURL is passed, this function will return all annotations in the database.
   * @param videoURL        The video for which annotations should be loaded
   * @param target          The string to which each annotation's target parameter is to be set
   * @param callbackMethod  A method that is executed after the information is loaded. This method
   *                        should accept a parameter which will contain all footnotes at callback time.
   *                        The footnotes have the target parameter set to 'footnote-container'.
   */
  this.loadAnnotationsForVideo = function(videoURL, target, callbackMethod){
    var sparqlCommand = prefixes + '\n\
      SELECT * WHERE { ?annotation oa:hasTarget ?videoURLWithFragments ; \n\
                             ?predicate ?object . \n\
                             ?object ?predicate2 ?object2 . \n\
                        BIND(xsd:integer(REPLACE(?annotation, "' + sm + 'Annotation", "", "i")) AS ?annotationID) . \n\
                        OPTIONAL { ?object2 ?predicate3 ?object3 . } \n\
                        FILTER CONTAINS(?videoURLWithFragments, "' + videoURL + '") . \n\
      } ORDER BY ?annotationID ?object ?object2 ?predicate3';
      // Below, you see the nice version of the query above but it unfortunately does not work.
      // This is due to standard Marmotta being unable to handle the SPARQL standard correctly.
      // Instead, we have to use the 'dumbed down' version you see above that requires more
      // processing work after the results were received.
      // 'SELECT ?videoURLWithFragments ?body ?content ?highlightedWord ?beginIndex ?endIndex ?resource \n\
      //  WHERE { ?annotation rdf:type oa:Annotation; \n\
      //                      oa:hasTarget ?videoURLWithFragments ; \n\
      //                      oa:hasBody ?body . \n\
      //                      FILTER CONTAINS(?videoURLWithFragments, "' + videoURL + '") . \n' +
      //          // Depending on the type of annotation, different fields can be expected.
      //          // There are three cases to cover:
      //          //   1. A simple annotation with some text as content
      //          //   2. A tag annotation that refers to an entity
      //          //   3. A complex NIF annotation
      //          // To cover all of them we have to combine two different queries (NIF is
      //          // essentially just an extension of a simple annotation).
      //          '{ { ?body foaf:page ?content } \n\
      //          UNION { \n\
      //            ?body cnt:chars ?content . \n\
      //            OPTIONAL { ?body oa:hasSelector ?selector . \n\
      //                       ?selector nif:anchorOf ?highlightedWord ; \n\
      //                       nif:beginIndex ?beginIndex ; \n\
      //                       nif:endIndex ?endIndex ; \n\
      //                       foaf:page ?resource . } \n\
      //          } } \n\
      // } ORDER BY ?body ?highlightedWord';
    sendSparqlSelectCommand( sparqlCommand ,
      function(result){
        var annotations = [];
        var currentAnnotationID = -1;
        var currentSelector;
        var newAnnotation;
        var newHighlightedWord;
        var numberOfSelectors = 0;
        for(var i = 0; i < result.length; i++){
          if(currentAnnotationID !== result[i].annotationID.value){
            currentAnnotationID = result[i].annotationID.value;
            newAnnotation = {};
            newAnnotation.id = currentAnnotationID;
            numberOfSelectors = 0;
            currentSelector = '';
            newHighlightedWord = {};
            // Convert the media fragment time notation into usable integers
            var time = result[i].videoURLWithFragments.value.match(/#t=\d*,\d*/g);
            if(time && time.length > 0){
              // Remove '#t=' from the string and split it into start and end time
              time = time[time.length - 1].substring(3).split(",");
              newAnnotation.start = time[0];
              newAnnotation.end   = time[1];
            }
            var location = result[i].videoURLWithFragments.value.match(/%x\d*.\d*.\d*.\d*/g);
            if (location !== null && location.length > 0) {
              // Remove '%x' from the string and split it into top, left, height and width
              location = location[location.length - 1].substring(2).split(".");
              location = {
                top : parseInt(location[0]) / 100,
                left : parseInt(location[1]) / 100,
                width : parseInt(location[2]) / 100,
                height : parseInt(location[3]) / 100
              };
            } else {
              location = {
                top : 0,
                left : 0,
                height : 0,
                width : 0
              };
            }
            newAnnotation.location = location;
            newAnnotation.target = target;
            newAnnotation.highlightedWords = [];
          }
          if(result[i].predicate2.value === 'http://www.w3.org/2011/content#chars' ||
             result[i].predicate2.value === foaf + 'page'){
            newAnnotation.text = result[i].object2.value;
          } else if (result[i].predicate3 !== undefined &&
                     result[i].predicate2.value === 'http://www.openannotation.org/ns/hasSelector'){
            if (result[i].predicate3.value === nifCore + 'anchorOf'){
              // anchorOf is the first relevant predicate we come across with alphabetical
              // ordering of the predicate column so this is the correct time to make a
              // new object for highlightedWord
              newHighlightedWord = {};
              newHighlightedWord.content = result[i].object3.value;
            } else if (result[i].predicate3.value === nifCore + 'beginIndex'){
              newHighlightedWord.beginIndex = result[i].object3.value;
            } else if (result[i].predicate3.value === nifCore + 'endIndex'){
              newHighlightedWord.endIndex = result[i].object3.value;
            } else if (result[i].predicate3.value === foaf + 'page'){
              newHighlightedWord.referral = result[i].object3.value;
              // foaf:page is the last relevant predicate we come across with alphabetical
              // ordering of the predicate column so at this point the newhighlightedWord
              // is filled up with all relevant details and can be pushed
              newAnnotation.highlightedWords.push(newHighlightedWord);
            }
          }
          // Add the new annotation when we have dealt with all bodies of the
          // current annotation or when it is the last part of the result.
          if (i + 1 === result.length || result[i + 1].annotationID.value !== currentAnnotationID) {
            annotations.push(newAnnotation);
          }
          // Below is the old way of dealing with the query results when we were still able
          // to use a simpler version of the nice version of the SPARQL query displayed
          // at the beginning of this method.
          // if(currentBody !== result[i].body.value){
          //   currentBody = result[i].body.value;
          //   newAnnotation = {};
          //   // Convert the media fragment time notation into usable integers
          //   var time = result[i].videoURLWithFragments.value.match(/#t=\d*,\d*/g);
          //   if(time && time.length > 0){
          //     // Remove '#t=' from the string and split it into start and end time
          //     time = time[time.length - 1].substring(3).split(",");
          //     newAnnotation.start = time[0];
          //     newAnnotation.end   = time[1];
          //   }
          //   var location = result[i].videoURLWithFragments.value.match(/%x\d*.\d*.\d*.\d*/g);
          //   if (location !== null && location.length > 0) {
          //     // Remove '%x' from the string and split it into x, y, height and width
          //     location = location[location.length - 1].substring(2).split(".");
          //     location = {
          //       top : parseInt(location[0]) / 100,
          //       left : parseInt(location[1]) / 100,
          //       height : parseInt(location[2]) / 100,
          //       width : parseInt(location[3]) / 100
          //     };
          //   } else {
          //     location = {
          //       top : 0,
          //       left : 0,
          //       height : 0,
          //       width : 0
          //     };
          //   }
          //   newAnnotation.location = location;
          //   newAnnotation.text = result[i].content.value;
          //   newAnnotation.target = target;
          //   newAnnotation.highlightedWords = [];
          // } else {
          //   // Because of the order in the SELECT query, we can assume that the
          //   // first body result is always without a selector, so if currentBody
          //   // is the same as the body of the current result, it has to be a selector.
          //   newAnnotation.highlightedWords.push({
          //     beginIndex: result[i].beginIndex.value,
          //     endIndex:   result[i].endIndex.value,
          //     content:    result[i].highlightedWord.value,
          //     referral:   result[i].resource.value
          //   });
          // }
          // // Add the new annotation when we have dealt with all bodies of the
          // // current annotation or when it is the last part of the result.
          // if (i + 1 === result.length || result[i + 1].body.value !== currentBody) {
          //   annotations.push(newAnnotation);
          // }
        }
        console.log(annotations);
        callbackMethod(annotations);
    });
  };

  /**
   * Create a string in the turtle format that includes all triples for the specified video URL.
   * Note: If no videoURL is passed, the resulting turtle string will contain all annotations
   *       in the database.
   * @param videoURL        The video for which annotations should be loaded
   * @param callbackMethod  A method that is executed after the turtle string was created.
   *                        This method should accept a parameter which will contain the turtle string.
   */
  this.exportAllAnnotationsForVideo = function(videoURL, callbackMethod){
    // First, find out all the IDs of the annotations that are relevant for us.
    var selectString = prefixes + 'SELECT ?annotationID \n\
      WHERE { ?annotation oa:hasTarget ?videoURLWithFragments . \n\
      BIND(xsd:integer(REPLACE(?annotation, "' + sm + 'Annotation", "", "i")) AS ?annotationID) . \n\
      FILTER CONTAINS(?videoURLWithFragments, "' + videoURL + '") . }';
    sendSparqlSelectCommand(selectString, function(sparqlResult){
      var turtleString = '';
      var databaseInTurtleRequest = new XMLHttpRequest();
      databaseInTurtleRequest.onload = function(turtleResult){
        var databaseInTurtle = turtleResult.target.responseText;
        var annotation = 'sm:Annotation';
        // Small Marmotta quirk: It includes '<http://www.w3.org/ns/ldp#>' randomly into the
        // resulting turtle string.
        var positionOfNSLDP = databaseInTurtle.indexOf('<http://www.w3.org/ns/ldp#>');
        // Go through all relevant annotations by using the IDs the SPARQL query returned.
        for(var i = 0; i < sparqlResult.length; i++){
          var annotationID = Number(sparqlResult[i].annotationID.value);
          var positionOfCurrentAnnotation = databaseInTurtle.indexOf(annotation + annotationID);
          // Small trick: indexOf only delivers the index of the first result so for the
          // position of the nextAnnotation we only look at the part of the string that
          // comes after the current annotation.
          var positionOfNextAnnotation = databaseInTurtle.indexOf(annotation, positionOfCurrentAnnotation + 1);
          // Extract the part of the turtle string that contains information about the current
          // annotation. Do so by taking the substring from the current annotation until the next
          // annotation or until '<http://www.w3.org/ns/ldp#>' (Marmotta quirk) or
          // until the end of the file.
          if(positionOfNextAnnotation !== -1 && ((positionOfCurrentAnnotation < positionOfNSLDP && positionOfNextAnnotation < positionOfNSLDP) || positionOfCurrentAnnotation > positionOfNSLDP)){
              turtleString += databaseInTurtle.substring(positionOfCurrentAnnotation, positionOfNextAnnotation);
          } else if (positionOfCurrentAnnotation < positionOfNSLDP){
            turtleString += databaseInTurtle.substring(positionOfCurrentAnnotation, positionOfNSLDP);
          } else {
            turtleString += databaseInTurtle.substring(positionOfCurrentAnnotation);
          }
        }
        console.log(turtleString);
        callbackMethod(turtleString);
      };
      databaseInTurtleRequest.open("get", "http://37.120.177.56:8080/marmotta/export/download?format=application%2Fx-turtle", true);
      databaseInTurtleRequest.send();
    });
  };
}

var annotationClient = new AnnotationClient();
